package json

func NewDecodingError(m string) *DecodingError {
	return &DecodingError{
		message: m,
	}
}

type DecodingError struct {
	message string
}

func (e *DecodingError) Error() string {
	return e.message
}
