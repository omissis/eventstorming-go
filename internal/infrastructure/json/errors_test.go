// +build unit

package json_test

import (
	"eventstorming-go/internal/infrastructure/json"
	"eventstorming-go/test"
	"testing"
)

func TestNewDecodingError(t *testing.T) {
	err := json.NewDecodingError("foo bar baz")

	test.Equals(t, "foo bar baz", err.Error())
}
