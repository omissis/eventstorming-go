package kit

import (
	"context"
	"encoding/json"
	"eventstorming-go/internal/domain"
	eshttp "eventstorming-go/internal/infrastructure/http"
	esjson "eventstorming-go/internal/infrastructure/json"
	"eventstorming-go/internal/infrastructure/jsonapi"
	"eventstorming-go/internal/infrastructure/reflect"
	"fmt"
	"net/http"
	"time"

	"gopkg.in/go-playground/validator.v9"

	"github.com/go-kit/kit/endpoint"
	httptransport "github.com/go-kit/kit/transport/http"
)

type workshop struct {
	ID        string    `json:"id"`
	Name      string    `json:"name"`
	CreatedAt time.Time `json:"created_at"`
}

type startWorkshopRequest struct {
	Name string `json:"name" validate:"required"`
}

type startWorkshopResponse struct {
	Err      string   `json:"err,omitempty"`
	Workshop workshop `json:"workshop"`
}

func makeStartWorkshopEndpoint(sw *eventstorming.StartWorkshop) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		if request == nil {
			return jsonapi.NewGoErrorResponse(fmt.Errorf("Empty request")), nil
		}

		req, ok := request.(startWorkshopRequest)

		if !ok {
			return jsonapi.NewGoErrorResponse(esjson.NewDecodingError("Cannot decode request")), nil
		}

		v := validator.New()

		err := v.Struct(req)

		if err != nil {
			if reflect.TypeOf(err) == "ValidationErrors" {
				return jsonapi.NewValidatorErrorResponse(err.(validator.ValidationErrors)), nil
			}

			return jsonapi.NewGoErrorResponse(err), nil
		}

		w := sw.Execute(req.Name)

		return startWorkshopResponse{
			Workshop: workshop{
				ID:        string(w.ID()),
				Name:      w.Name(),
				CreatedAt: w.CreatedAt(),
			},
		}, nil
	}
}

func NewStartWorkshopHttpHandler(
	clock eventstorming.Clock,
	idgen eventstorming.IDGenerator,
	wsrepo eventstorming.WorkshopRepository,
) http.Handler {
	server := httptransport.NewServer(
		makeStartWorkshopEndpoint(eventstorming.NewStartWorkshop(clock, idgen, wsrepo)),
		decodeStartWorkshopRequest,
		encodeStartWorkshopResponse,
	)

	return eshttp.NewMethodHandlerDecorator([]string{"POST"}, server)
}

func decodeStartWorkshopRequest(_ context.Context, req *http.Request) (interface{}, error) {
	var request startWorkshopRequest
	if err := json.NewDecoder(req.Body).Decode(&request); err != nil {
		return err, nil
	}
	return request, nil
}

func encodeStartWorkshopResponse(_ context.Context, w http.ResponseWriter, resp interface{}) error {
	return json.NewEncoder(w).Encode(resp)
}
