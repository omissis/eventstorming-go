// +build unit

package kit_test

import (
	"eventstorming-go/internal/infrastructure/kit"
	esstrings "eventstorming-go/internal/infrastructure/strings"
	"eventstorming-go/test"
	"eventstorming-go/test/mock"
	"fmt"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestNewStartWorkshopHttpHandler(t *testing.T) {
	clock := mock.NewClock(test.DefaultDate())
	idgen := mock.NewUUIDGenerator(test.DefaultUUID)
	wsrepo := mock.NewInMemoryWorkshopRepository()
	handler := kit.NewStartWorkshopHttpHandler(clock, idgen, wsrepo)

	b := strings.NewReader("{\"name\":\"Foo bar\"}")
	w := httptest.NewRecorder()
	r := httptest.NewRequest("POST", "https:/eventstorming.dev", b)

	handler.ServeHTTP(w, r)

	expectedBody := fmt.Sprintf(
		`{"workshop":{"id":"%s","name":"%s","created_at":"%s"}}`,
		test.DefaultUUID,
		"Foo bar",
		test.DefaultDateStr)

	actualBody := strings.TrimSpace(w.Body.String())

	test.Equals(t, expectedBody, actualBody)
}

func TestNewStartWorkshopHttpHandlerUnsupportedMethods(t *testing.T) {
	clock := mock.NewClock(test.DefaultDate())
	idgen := mock.NewUUIDGenerator(test.DefaultUUID)
	wsrepo := mock.NewInMemoryWorkshopRepository()
	handler := kit.NewStartWorkshopHttpHandler(clock, idgen, wsrepo)

	for _, method := range []string{"GET", "HEAD", "PUT", "DELETE"} {
		t.Run(method, func(t *testing.T) {
			b := strings.NewReader("{\"name\":\"Foo bar\"}")
			w := httptest.NewRecorder()
			r := httptest.NewRequest(method, "https://api.eventstorming.dev", b)

			handler.ServeHTTP(w, r)

			test.Equals(t, 405, w.Code)
		})
	}
}

func TestNewStartWorkshopHttpHandlerInputError(t *testing.T) {
	clock := mock.NewClock(test.DefaultDate())
	idgen := mock.NewUUIDGenerator(test.DefaultUUID)
	wsrepo := mock.NewInMemoryWorkshopRepository()
	handler := kit.NewStartWorkshopHttpHandler(clock, idgen, wsrepo)

	for _, tt := range wrongRequestBodyTests {
		t.Run(tt.name, func(t *testing.T) {
			b := strings.NewReader(tt.in.(string))
			w := httptest.NewRecorder()
			r := httptest.NewRequest("POST", "https:/eventstorming.dev", b)

			handler.ServeHTTP(w, r)

			actualBody := strings.TrimSpace(w.Body.String())

			test.Equals(t, tt.out, actualBody)
		})
	}
}

var wrongRequestBodyTests = []struct {
	name string
	in   interface{}
	out  string
}{
	{
		"wrong property",
		`{"wrong_property":"Wrong Value"}`,
		esstrings.StripWhitespace(
			fmt.Sprintf(
				`{
					"errors": [
						{
							"status": "422",
							"source": { "pointer": "/name" },
							"title":  "ValidationErrors",
							"detail": "Key: 'startWorkshopRequest.Name' Error:Field validation for 'Name' failed on the 'required' tag"
						}
					]
				}`,
			),
		),
	},
	{
		"empty body",
		"",
		esstrings.StripWhitespace(
			fmt.Sprintf(
				`{
					"errors": [
						{
							"status": "400",
							"source": { "pointer": "/" },
							"title":  "DecodingError",
							"detail": "Cannot decode request"
						}
					]
				}`,
			),
		),
	},
}
