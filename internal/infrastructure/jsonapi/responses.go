package jsonapi

import (
	"eventstorming-go/internal/infrastructure/reflect"
	"fmt"
	"strings"

	"gopkg.in/go-playground/validator.v9"
)

// ErrorResponse ///////////////////////////////////////////////////////////////////////////////////////////////////////

// NewValidatorErrorResponse returns an ErrorResponse value from a validator's errors value
func NewValidatorErrorResponse(err validator.ValidationErrors) *ErrorResponse {
	errs := []Error{}
	for _, fielderr := range err {
		pointer := ""
		parts := strings.Split(fielderr.Namespace(), ".")

		for i := 1; i < len(parts); i++ {
			pointer += "/" + strings.ToLower(parts[i])
		}

		errs = append(errs, *NewError(422, err, pointer))
	}

	return NewErrorResponse(errs)
}

// NewGoErrorResponse returns an ErrorResponse value from a common go error
func NewGoErrorResponse(err error) *ErrorResponse {
	jerr := *NewError(400, err, "/")

	return NewErrorResponse([]Error{jerr})
}

// NewErrorResponse returns an ErrorResponse value from an array of jsonapi errors
func NewErrorResponse(errs []Error) *ErrorResponse {
	var errors []Error

	for _, err := range errs {
		errors = append(errors, err)
	}

	return &ErrorResponse{
		Errors: errors,
	}
}

// ErrorResponse represents the data for a jsonapi error response
type ErrorResponse struct {
	Errors []Error `json:"errors"`
}

// Error ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

// NewError returns a new jsonapi error value
func NewError(status int, err error, pointer string) *Error {
	return &Error{
		Status: fmt.Sprintf("%d", status),
		Source: map[string]string{"pointer": pointer},
		Title:  reflect.TypeOf(err),
		Detail: err.Error(),
	}
}

// Error describes an undesired condition in the system
// See: https://jsonapi.org/examples/#error-objects
type Error struct {
	Status string            `json:"status"`
	Source map[string]string `json:"source"`
	Title  string            `json:"title"`
	Detail string            `json:"detail"`
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
