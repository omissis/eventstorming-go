// +build unit

package jsonapi_test

import (
	"eventstorming-go/internal/infrastructure/jsonapi"
	"eventstorming-go/test"
	"testing"

	"gopkg.in/go-playground/validator.v9"
)

func TestNewValidatorErrorResponse(t *testing.T) {
	v := validator.New()
	f := Foo{}

	err := v.Struct(f).(validator.ValidationErrors)

	resp := jsonapi.NewValidatorErrorResponse(err)

	test.Assert(t, len(resp.Errors) == 2, "There should be two validation errors in the response")
	test.Equals(t, "/one", resp.Errors[0].Source["pointer"])
	test.Equals(t, "/three/four", resp.Errors[1].Source["pointer"])
}

type Foo struct {
	One   string `json:"one" validate:"required"`
	Two   int    `json:"two"`
	Three Bar    `json:"three"`
}

type Bar struct {
	Four float64 `json:"four" validate:"required"`
	Five string  `json:"four"`
}
