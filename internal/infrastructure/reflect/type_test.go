// +build unit

package reflect_test

import (
	"eventstorming-go/internal/infrastructure/reflect"
	"eventstorming-go/test"
	"testing"
)

func TestTypeOf(t *testing.T) {
	for _, tt := range typeTests {
		t.Run(tt.out, func(t *testing.T) {
			test.Equals(t, tt.out, reflect.TypeOf(tt.in))
		})
	}
}

var typeTests = []struct {
	in  interface{}
	out string
}{
	{"foobar", "string"},
	{123, "int"},
	{FooError{}, "FooError"},
	{&FooError{}, "FooError"},
}

type FooError struct{}

func (e *FooError) Error() string {
	return "foo error"
}
