package reflect

import (
	goreflect "reflect"
)

// TypeOf returns the name of the type of the passed argument
// If the value is a pointer, the function follows it until it gets to the actual struct type
func TypeOf(i interface{}) string {
	t := goreflect.TypeOf(i)
	for t.Kind() == goreflect.Ptr {
		t = t.Elem()
	}
	return t.Name()
}
