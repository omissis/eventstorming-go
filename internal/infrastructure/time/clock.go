package time

import gotime "time"

func NewClock() *Clock {
	return &Clock{}
}

type Clock struct {
}

func (c *Clock) Now() gotime.Time {
	return gotime.Now()
}
