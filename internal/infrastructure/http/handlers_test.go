// +build unit

package http_test

import (
	"eventstorming-go/internal/infrastructure/http"
	"eventstorming-go/test"
	"fmt"
	"io/ioutil"
	gohttp "net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestMethodHandlerDecorator(t *testing.T) {
	for i, tt := range methodHandlerTests {
		t.Run(fmt.Sprintf("test %d", i), func(t *testing.T) {
			b := strings.NewReader(tt.requestBody)
			r := httptest.NewRequest(tt.requestMethod, "https:/eventstorming.dev", b)
			w := httptest.NewRecorder()

			decoratedHandler := http.NewMethodHandlerDecorator(tt.supportedMethods, &TestHttpHandler{t: t})

			decoratedHandler.ServeHTTP(w, r)

			test.Equals(t, tt.expectedBody, w.Body.String())
		})
	}
}

var methodHandlerTests = []struct {
	requestMethod    string
	requestBody      string
	supportedMethods []string
	expectedBody     string
}{
	{"POST", "foo bar baz quux", []string{"POST"}, "foo bar baz quux"},
	{"GET", "foo bar baz quux", []string{"POST"}, "Method GET is not allowed. Allowed methods are: [POST]"},
}

// TestHttpHandler echoes what receives from the body of the request.
type TestHttpHandler struct {
	t *testing.T
}

func (thh *TestHttpHandler) ServeHTTP(w gohttp.ResponseWriter, r *gohttp.Request) {
	var b []byte

	b, reqErr := ioutil.ReadAll(r.Body)

	if reqErr != nil {
		thh.t.Error(reqErr)

		return
	}

	_, respErr := w.Write(b)

	if respErr != nil {
		thh.t.Error(respErr)

		return
	}
}
