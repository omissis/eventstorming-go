package http

import (
	"eventstorming-go/internal/infrastructure/strings"
	"fmt"
	"log"
	gohttp "net/http"
)

// MethodHandlerDecorator //////////////////////////////////////////////////////////////////////////////////////////////

func NewMethodHandlerDecorator(methods []string, handler gohttp.Handler) *MethodHandlerDecorator {
	return &MethodHandlerDecorator{
		allowedMethods: methods,
		handler:        handler,
	}
}

type MethodHandlerDecorator struct {
	allowedMethods []string
	handler        gohttp.Handler
}

func (mhd *MethodHandlerDecorator) ServeHTTP(w gohttp.ResponseWriter, r *gohttp.Request) {
	if !strings.SliceContains(mhd.allowedMethods, r.Method) {
		w.WriteHeader(405)

		_, err := w.Write([]byte(
			fmt.Sprintf("Method %s is not allowed. Allowed methods are: %v", r.Method, mhd.allowedMethods),
		))

		if err != nil {
			log.Println(err)
		}

		return
	}

	mhd.handler.ServeHTTP(w, r)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
