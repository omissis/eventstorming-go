package sqlx

import (
	"eventstorming-go/internal/domain"
	"time"

	"github.com/jmoiron/sqlx"
)

type workshop struct {
	ID        string    `db:"uuid"`
	Name      string    `db:"name"`
	CreatedAt time.Time `db:"created_at"`
}

func NewWorkshopRepository(db *sqlx.DB) *WorkshopRepository {
	return &WorkshopRepository{
		db: db,
	}
}

type WorkshopRepository struct {
	db *sqlx.DB
}

func (wr *WorkshopRepository) Add(w eventstorming.Workshop) error {
	q := `INSERT INTO workshop(
		uuid,
		name,
		created_at
	) VALUES (
		:uuid,
		:name,
		:created_at
	)`

	_, err := wr.db.NamedExec(q, workshop{
		ID:        string(w.ID()),
		Name:      w.Name(),
		CreatedAt: w.CreatedAt(),
	})

	return err
}

func (wr *WorkshopRepository) GetById(ID eventstorming.WorkshopID) (eventstorming.Workshop, error) {
	dbws := workshop{}

	if err := wr.db.Get(&dbws, "SELECT * FROM workshop WHERE uuid=$1", string(ID)); err != nil {
		return eventstorming.Workshop{}, err
	}

	return eventstorming.NewWorkshop(eventstorming.WorkshopID(dbws.ID), dbws.Name, dbws.CreatedAt), nil
}
