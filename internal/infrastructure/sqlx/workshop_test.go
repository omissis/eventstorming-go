// +build integration

package sqlx_test

import (
	"eventstorming-go/internal/domain"
	"eventstorming-go/internal/infrastructure/sqlx"
	"eventstorming-go/test"
	"log"
	"os"
	"testing"
	"time"

	_ "github.com/go-sql-driver/mysql"
	gosqlx "github.com/jmoiron/sqlx"
)

var database *gosqlx.DB
var repository *sqlx.WorkshopRepository

func TestMain(m *testing.M) {
	database = test.GetSQLDb()

	defer func() {
		if err := database.Close(); err != nil {
			log.Fatalf(err.Error())
		}
	}()

	repository = sqlx.NewWorkshopRepository(database)

	exitCode := m.Run()

	if err := database.Close(); err != nil {
		log.Fatalf(err.Error())
	}

	os.Exit(exitCode)
}

func TestItAddsToTheDatabase(t *testing.T) {
	test.CleanUpDb(database)

	now := time.Now().UTC()

	workshop := eventstorming.NewWorkshop(test.DefaultUUID, "foo bar baz quux", now)
	if err := repository.Add(workshop); err != nil {
		t.Fatal(err)
	}

	var id, name string
	var createdAt time.Time

	const query = `SELECT uuid, name, created_at FROM workshop WHERE uuid = ?`
	if err := database.QueryRow(query, test.DefaultUUID).Scan(&id, &name, &createdAt); err != nil {
		log.Fatal(err)
	}

	test.Equals(t, test.DefaultUUID, id)
	test.Equals(t, "foo bar baz quux", name)
	test.Equals(t, now.Unix(), createdAt.Unix())
}
