package strings

import "regexp"

func SliceContains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

func StripWhitespace(s string) string {
	r := regexp.MustCompile(`("(?:[^"\\]|\\.)*")|[\s\t\r\n ]+`)

	return r.ReplaceAllString(s, "$1")
}
