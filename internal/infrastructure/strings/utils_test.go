// +build unit

package strings_test

import (
	"eventstorming-go/internal/infrastructure/strings"
	"eventstorming-go/test"
	"testing"
)

func TestStripWhitespace(t *testing.T) {
	for _, tt := range stripWhitespaceTests {
		t.Run(tt.in, func(t *testing.T) {
			test.Equals(t, tt.out, strings.StripWhitespace(tt.in))
		})
	}
}

var stripWhitespaceTests = []struct {
	in  string
	out string
}{
	{`"	foo		bar	"`, `"	foo		bar	"`},
	{
		`
		{
			"key":	"	foo		bar baz "
		}`,
		`{"key":"	foo		bar baz "}`,
	},
}
