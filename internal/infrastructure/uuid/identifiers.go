package uuid

import (
	"eventstorming-go/internal/domain"

	gouuid "github.com/satori/go.uuid"
)

func NewUUID(u string) *UUID {
	return &UUID{
		uuid: gouuid.FromStringOrNil(u),
	}
}

type UUID struct {
	eventstorming.ID
	uuid gouuid.UUID
}

func (u *UUID) String() string {
	return u.uuid.String()
}

func NewUUIDv4Generator() *UUIDv4Generator {
	return &UUIDv4Generator{}
}

type UUIDv4Generator struct {
	eventstorming.IDGenerator
}

func (idgen *UUIDv4Generator) Generate() eventstorming.ID {
	return gouuid.NewV4()
}
