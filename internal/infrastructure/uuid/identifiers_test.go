// +build unit

package uuid_test

import (
	"eventstorming-go/internal/infrastructure/uuid"
	"eventstorming-go/test"
	"testing"
)

func TestUUIDToString(t *testing.T) {
	id := uuid.NewUUID(test.DefaultUUID)

	test.Equals(t, test.DefaultUUID, id.String())
}

func TestNewUUIDv4Generator(t *testing.T) {
	idgen := uuid.NewUUIDv4Generator()

	u := idgen.Generate()

	test.Assert(t, len(u.String()) == 36, "uuid must be exactly 36-characters long.")
}
