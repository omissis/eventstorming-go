// +build unit

package eventstorming_test

import (
	"eventstorming-go/internal/domain"
	"eventstorming-go/test"
	"eventstorming-go/test/mock"
	"testing"
	"time"
)

func TestAWorkshopCanBeStarted(t *testing.T) {
	now := time.Now()
	clock := mock.NewClock(now)
	idgen := mock.NewUUIDGenerator(test.DefaultUUID)
	repo := mock.NewInMemoryWorkshopRepository()

	startWorkshop := eventstorming.NewStartWorkshop(clock, idgen, repo)
	ws := startWorkshop.Execute("Acme Inc")

	test.Assert(t, ws.Name() == "Acme Inc", "workshop name must be Acme Inc")
	test.Assert(t, ws.CreatedAt() == now, "workshop creation time(%v) does not match the expected one(%v)", ws.CreatedAt, now)

	saved, err := repo.GetById(test.DefaultUUID)

	if err != nil {
		t.Error(err)
	}

	test.Assert(t, ws == saved, "workshop should have been added to the repository")
}
