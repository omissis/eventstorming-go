package eventstorming

type ID interface {
	String() string
}

type IDGenerator interface {
	Generate() ID
}
