package eventstorming

import (
	"fmt"
	"time"
)

// Workshop ////////////////////////////////////////////////////////////////////////////////////////////////////////////

type WorkshopID string

func NewWorkshop(ID WorkshopID, name string, createdAt time.Time) Workshop {
	return Workshop{
		id:        ID,
		name:      name,
		createdAt: createdAt,
	}
}

type Workshop struct {
	id        WorkshopID
	name      string
	createdAt time.Time
}

func (w *Workshop) ID() WorkshopID {
	return w.id
}

func (w *Workshop) Name() string {
	return w.name
}

func (w *Workshop) CreatedAt() time.Time {
	return w.createdAt
}

// WorkshopRepository //////////////////////////////////////////////////////////////////////////////////////////////////

type WorkshopRepository interface {
	Add(workshop Workshop) error

	GetById(ID WorkshopID) (Workshop, error)
}

// StartWorkshop ///////////////////////////////////////////////////////////////////////////////////////////////////////

func NewStartWorkshop(c Clock, ig IDGenerator, wr WorkshopRepository) *StartWorkshop {
	return &StartWorkshop{
		clock:  c,
		idgen:  ig,
		wsrepo: wr,
	}
}

type StartWorkshop struct {
	clock  Clock
	idgen  IDGenerator
	wsrepo WorkshopRepository
}

func (w *StartWorkshop) Execute(name string) Workshop {
	ws := NewWorkshop(WorkshopID(w.idgen.Generate().String()), name, w.clock.Now())

	err := w.wsrepo.Add(ws)

	if err != nil {
		fmt.Println(err)
		return Workshop{}
	}

	return ws
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
