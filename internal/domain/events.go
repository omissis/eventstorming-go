package eventstorming

import "time"

type WorkshopStarted struct {
	ID WorkshopID
	At time.Time
}
