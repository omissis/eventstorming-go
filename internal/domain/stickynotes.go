package eventstorming

import "image/color"

// Constants ///////////////////////////////////////////////////////////////////////////////////////////////////////////

const (
	small  = Size("small")
	medium = Size("medium")
	large  = Size("large")
)

var (
	lightBlue    = color.RGBA{R: 48, G: 172, B: 251, A: 255}  // command
	yellow       = color.RGBA{R: 255, G: 236, B: 142, A: 255} // aggregate
	orange       = color.RGBA{R: 252, G: 140, B: 37, A: 255}  // domain event
	liliac       = color.RGBA{R: 238, G: 225, B: 254, A: 255} // process/policy
	pink         = color.RGBA{R: 251, G: 213, B: 235, A: 255} // external system
	green        = color.RGBA{R: 154, G: 191, B: 57, A: 255}  // view model
	white        = color.RGBA{R: 254, G: 254, B: 254, A: 255} // ui
	brightYellow = color.RGBA{R: 253, G: 246, B: 71, A: 255}  // role
)

// StickyNote ///////////////////////////////////////////////////////////////////////////////////////////////////////////

type Size string

type StickyNote struct {
	content string
	color   color.Color
	size    Size
}

func (sn *StickyNote) Content() string {
	return sn.content
}

func (sn *StickyNote) Color() color.Color {
	return sn.color
}

func (sn *StickyNote) Size() Size {
	return sn.size
}

// StickyNote - Command ////////////////////////////////////////////////////////////////////////////////////////////////

func NewCommand(content string) Command {
	return Command{
		StickyNote{
			content: content,
			color:   lightBlue,
			size:    medium,
		},
	}
}

type Command struct {
	StickyNote
}

// StickyNote - Aggregate //////////////////////////////////////////////////////////////////////////////////////////////

func NewAggregate(content string) Aggregate {
	return Aggregate{
		StickyNote{
			content: content,
			color:   yellow,
			size:    medium,
		},
	}
}

type Aggregate struct {
	StickyNote
}

// StickyNote - External System ////////////////////////////////////////////////////////////////////////////////////////

func NewExternalSystem(content string) ExternalSystem {
	return ExternalSystem{
		StickyNote{
			content: content,
			color:   pink,
			size:    large,
		},
	}
}

type ExternalSystem struct {
	StickyNote
}

// StickyNote - DomainEvent ////////////////////////////////////////////////////////////////////////////////////////////

func NewDomainEvent(content string) DomainEvent {
	return DomainEvent{
		StickyNote{
			content: content,
			color:   orange,
			size:    medium,
		},
	}
}

type DomainEvent struct {
	StickyNote
}

// StickyNote - Read Model /////////////////////////////////////////////////////////////////////////////////////////////

func NewReadModel(content string) ReadModel {
	return ReadModel{
		StickyNote{
			content: content,
			color:   green,
			size:    medium,
		},
	}
}

type ReadModel struct {
	StickyNote
}

// StickyNote - UI /////////////////////////////////////////////////////////////////////////////////////////////////////

func NewUI(content string) UI {
	return UI{
		StickyNote{
			content: content,
			color:   white,
			size:    large,
		},
	}
}

type UI struct {
	StickyNote
}

// StickyNote - Process ////////////////////////////////////////////////////////////////////////////////////////////////

func NewProcess(content string) Process {
	return Process{
		StickyNote{
			content: content,
			color:   liliac,
			size:    medium,
		},
	}
}

type Process struct {
	StickyNote
}

// StickyNote - Role ///////////////////////////////////////////////////////////////////////////////////////////////////

func NewRole(content string) Role {
	return Role{
		StickyNote{
			content: content,
			color:   brightYellow,
			size:    small,
		},
	}
}

type Role struct {
	StickyNote
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
