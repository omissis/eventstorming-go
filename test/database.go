package test

import (
	"flag"
	"fmt"
	"log"
	"os"

	"github.com/jmoiron/sqlx"
)

func GetSQLDb() *sqlx.DB {
	assertIsRunningAsTest()

	driver := os.Getenv("DATABASE_DRIVER_NAME")
	source := os.Getenv("DATABASE_SOURCE_NAME")

	db, err := sqlx.Open(driver, source)
	if err != nil {
		log.Panic(err)
	}

	return db
}

func CleanUpDb(db *sqlx.DB) {
	assertIsRunningAsTest()

	queries := []string{
		"DELETE FROM workshop WHERE 1;",
	}

	for _, query := range queries {
		_, err := db.Exec(query)
		if err != nil {
			log.Panic(err)
		}
	}
}

func assertIsRunningAsTest() {
	if flag.Lookup("test.v") == nil {
		log.Panic(fmt.Errorf("these utils can be used only in tests"))
	}
}
