package mock

import (
	"time"
)

func NewClock(t time.Time) *Clock {
	return &Clock{time: t}
}

type Clock struct {
	time time.Time
}

func (c *Clock) Now() time.Time {
	return c.time
}
