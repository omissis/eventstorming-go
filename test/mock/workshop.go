package mock

import (
	"eventstorming-go/internal/domain"
	"fmt"
)

func NewInMemoryWorkshopRepository() eventstorming.WorkshopRepository {
	return &InMemoryWorkshopRepository{}
}

type InMemoryWorkshopRepository struct {
	workshops []eventstorming.Workshop
}

func (wr *InMemoryWorkshopRepository) Add(workshop eventstorming.Workshop) error {
	wr.workshops = append(wr.workshops, workshop)

	return nil
}

func (wr *InMemoryWorkshopRepository) GetById(ID eventstorming.WorkshopID) (eventstorming.Workshop, error) {
	for _, w := range wr.workshops {
		if w.ID() == ID {
			return w, nil
		}
	}

	return eventstorming.Workshop{}, fmt.Errorf("Cannot find Workshop with id: %s", ID)
}
