package mock

import (
	"eventstorming-go/internal/domain"
	"eventstorming-go/internal/infrastructure/uuid"
)

func NewUUIDGenerator(defaultUUID string) *UUIDGenerator {
	return &UUIDGenerator{
		defaultUUID: defaultUUID,
	}
}

type UUIDGenerator struct {
	eventstorming.IDGenerator
	defaultUUID string
}

func (idgen *UUIDGenerator) Generate() eventstorming.ID {
	return uuid.NewUUID(idgen.defaultUUID)
}
