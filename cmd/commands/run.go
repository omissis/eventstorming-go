package commands

import (
	"eventstorming-go/internal/infrastructure/kit"
	"eventstorming-go/internal/infrastructure/sqlx"
	"eventstorming-go/internal/infrastructure/time"
	"eventstorming-go/internal/infrastructure/uuid"
	"fmt"
	"net/http"
	"os"

	_ "github.com/go-sql-driver/mysql"
	gosqlx "github.com/jmoiron/sqlx"
	"github.com/spf13/cobra"
)

func NewRunCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "run",
		Short: "Run the eventstorming daemon",
		Args:  cobra.ExactArgs(0),
		RunE: func(cmd *cobra.Command, args []string) error {
			return run()
		},
	}
}

func run() error {
	printBanner()

	c := time.NewClock()
	idgen := uuid.NewUUIDv4Generator()

	db, err := gosqlx.Connect(os.Getenv("DATABASE_DRIVER_NAME"), os.Getenv("DATABASE_SOURCE_NAME"))
	if err != nil {
		return err
	}

	wsrepo := sqlx.NewWorkshopRepository(db)

	http.Handle("/start-workshop", kit.NewStartWorkshopHttpHandler(c, idgen, wsrepo))

	return http.ListenAndServe(":8080", nil)
}

func printBanner() {
	fmt.Println(`
	___________ _________________
	\_   _____//   _____/\______ \
	 |    __)_ \_____  \  |    |  \
	 |        \/        \ |    |   \
	/_______  /_______  //_______  /
	        \/        \/         \/
	      Event Storming Daemon`)
	fmt.Println()
}
