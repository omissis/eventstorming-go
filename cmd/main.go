package main

import (
	"eventstorming-go/cmd/commands"
	"log"

	"github.com/spf13/cobra"
)

func main() {
	var rootCmd = &cobra.Command{
		Use:   "eventstormingd",
		Short: "Event Storming Daemon",
	}

	rootCmd.AddCommand(commands.NewRunCmd())

	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}
