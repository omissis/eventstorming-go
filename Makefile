.PHONY: test

_GOLANGCI_LINT_VERSION ?= v1.23.6
_MIGRATE_VERSION ?= v4.9.1
_DATABASE_HOST ?= es_mysql
_DATABASE_DRIVER_NAME ?= mysql
_DATABASE_NAME ?= eventstormingd
_DATABASE_SOURCE_NAME ?= root:root@tcp(es_mysql:3306)/eventstormingd?parseTime=true
_MIGRATE_DATABASE_NAME ?= ${_DATABASE_NAME}
_MIGRATE_DATABASE_SOURCE_NAME ?= ${_DATABASE_SOURCE_NAME}
_MIGRATE_STEPS ?=
_TEST_DATABASE_NAME ?= eventstormingd_test
_TEST_DATABASE_SOURCE_NAME ?= root:root@tcp(es_mysql:3306)/eventstormingd_test?parseTime=true

base-env:
	@echo 'export DATABASE_DRIVER_NAME="${_DATABASE_DRIVER_NAME}"'

env: base-env
	@echo 'export DATABASE_SOURCE_NAME="${_DATABASE_SOURCE_NAME}"'

env-test: base-env
	@echo 'export DATABASE_SOURCE_NAME="${_TEST_DATABASE_SOURCE_NAME}"'

clean:
	@rm bin/eventstormingd

vendor:
	@go mod vendor

tools-lint:
	@curl -sfL https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | sh -s ${_GOLANGCI_LINT_VERSION}

tools: tools-lint

lint:
	@if [ ! -f bin/golangci-lint ]; then $(MAKE) tools-lint; fi
	@bin/golangci-lint run --print-issued-lines=false cmd internal test

build:
	@go build -a -o bin/eventstormingd cmd/main.go

run:
	@DATABASE_DRIVER_NAME="${_DATABASE_DRIVER_NAME}" DATABASE_SOURCE_NAME="${_DATABASE_SOURCE_NAME}" go run -a cmd/main.go run

test-unit:
	@go test --tags=unit --cover `go list ./... | grep -v /vendor/`

test-integration:
	@mysql --defaults-extra-file=conf/mysql/dev.cnf -h ${_DATABASE_HOST} -e "DROP DATABASE IF EXISTS ${_TEST_DATABASE_NAME};"

	@_MIGRATE_DATABASE_NAME="${_TEST_DATABASE_NAME}" _MIGRATE_DATABASE_SOURCE_NAME="${_TEST_DATABASE_SOURCE_NAME}" $(MAKE) migrate-up

	@DATABASE_DRIVER_NAME="${_DATABASE_DRIVER_NAME}" DATABASE_SOURCE_NAME="${_TEST_DATABASE_SOURCE_NAME}" GOMAXPROCS=1 CGO_ENABLED=0 go test --tags=integration --cover `go list ./... | grep -v /vendor/`

migrate-up:
	@mysql --defaults-extra-file=conf/mysql/dev.cnf -h "${_DATABASE_HOST}" -e "CREATE DATABASE IF NOT EXISTS ${_MIGRATE_DATABASE_NAME};"

	@docker run --rm \
		-v $(shell pwd)/migrations:/migrations \
		--network eventstorming_default \
		--link ${_DATABASE_HOST} \
		migrate/migrate:${_MIGRATE_VERSION} \
		-path=/migrations \
		-database "${_DATABASE_DRIVER_NAME}://${_MIGRATE_DATABASE_SOURCE_NAME}" up ${_STEPS}

migrate-down:
	@mysql --defaults-extra-file=conf/mysql/dev.cnf -h "${_DATABASE_HOST}" -e "CREATE DATABASE IF NOT EXISTS ${_MIGRATE_DATABASE_NAME};"

	@docker run --rm \
		-v $(shell pwd)/migrations:/migrations \
		--network eventstorming_default \
		--link ${_DATABASE_HOST} \
		migrate/migrate:${_MIGRATE_VERSION} \
		-path=/migrations \
		-database "${_DATABASE_DRIVER_NAME}://${_MIGRATE_DATABASE_SOURCE_NAME}" down ${_STEPS}
