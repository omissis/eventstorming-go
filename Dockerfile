FROM scratch AS production

COPY --from=migrate/migrate:v4.9.1 /migrate /bin/migrate
COPY ./migrations /migrations
COPY ./bin/eventstormingd /bin/eventstormingd

ENTRYPOINT bin/eventstormingd

FROM golang:1.13.8-alpine3.11 AS development

RUN apk update && \
    apk add gcc git libc-dev make ca-certificates && \
    rm -rf /var/cache/apk/*

WORKDIR /srv

CMD make run
